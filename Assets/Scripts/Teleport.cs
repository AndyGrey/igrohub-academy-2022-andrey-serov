using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour
{
    public GameObject entryCell;
    public GameObject exitCell;
    public GameObject player;
    
    private BoxCollider _entryCellCollider;
    private BoxCollider _playerCollider;
    // Start is called before the first frame update
    void Start()
    {
        _entryCellCollider = entryCell.GetComponent<BoxCollider>();
        _playerCollider = player.GetComponent<BoxCollider>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_entryCellCollider.bounds.Intersects(_playerCollider.bounds))
        {
            Vector3 p = exitCell.transform.position;
            player.transform.position = new Vector3(p.x, 0.5f, p.z);
        }
    }
}
