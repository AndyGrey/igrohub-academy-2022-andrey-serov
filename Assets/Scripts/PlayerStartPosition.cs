using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStartPosition : MonoBehaviour
{
    public GameObject startCell;

    public GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        Vector3 p = startCell.transform.position;
        player.transform.position = new Vector3(p.x, 0.5f, p.z);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
